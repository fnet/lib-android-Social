package com.bookbuf.service.client;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;

import com.bookbuf.service.R;
import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.social.handlers.ISSOHandler;
import com.bookbuf.social.handlers.IShareHandler;
import com.bookbuf.social.share.action.ShareAction;
import com.bookbuf.social.share.content.ShareContent;
import com.bookbuf.social.share.content.media.image.ImageMedia;
import com.bookbuf.social.share.content.media.image.util.BitmapUtils;

import java.lang.ref.WeakReference;
import java.util.Map;

public class MainActivity extends Activity {

	String TAG = getClass ().getSimpleName ();
	SocialAPI service;

	RadioGroup channel;

	PlatformEnum platform = null;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);
		setContentView (R.layout.activity_main);
		channel = (RadioGroup) findViewById (R.id.channel);
		service = SocialAPI.getInstance (this);


		loadChannel ();
		channel.setOnCheckedChangeListener (new RadioGroup.OnCheckedChangeListener () {
			@Override
			public void onCheckedChanged (RadioGroup group, int checkedId) {
				loadChannel ();
			}
		});
	}

	private void loadChannel () {
		switch (channel.getCheckedRadioButtonId ()) {
			case R.id.app_share_channel:
				platform = PlatformEnum.WX_SCENE;
				break;
			case R.id.app_share_channel_favorite:
				platform = PlatformEnum.WX_SCENE_FAVORITE;
				break;
			case R.id.app_share_channel_timeline:
				platform = PlatformEnum.WX_SCENE_TIMELINE;
				break;
			default:
				break;
		}
	}

	public void onClickAuth (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			default:
				break;
		}
		// 调用授权
		service.runOauthApply (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});

	}

	public void onClickAuthDel (View view) {
		PlatformEnum platform = null;
		switch (view.getId ()) {
			case R.id.app_auth_wechat:
				platform = PlatformEnum.WX_SCENE;
				break;
			default:
				break;
		}
		service.runOauthDelete (this, platform, new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
				Log.d (TAG, "[AuthListener] onError: action = " + action);
				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
			}
		});
	}


	private void shareText () {

		ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl ("[A]www.healthbok.com")
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();

		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		ShareAction action = new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm ("[B]www.healthbok.com")
				.setSharePlatform (platform)
				.setShareListener (shareListener);

		service.runShare (this, action, action.getShareListener ());
	}

	public void onClickShare (View view) {
		switch (view.getId ()) {
			case R.id.app_share_text:
				shareText ();
				break;
			case R.id.app_share_img_url:


				String less_than_32k = "http://www.healthbok.com/images/map.png";

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[url] 分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (new ImageMedia (new WeakReference<Context> (MainActivity.this), less_than_32k))
						.build ());

				break;
			case R.id.app_share_img_res:

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[res]分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (new ImageMedia (new WeakReference<Context> (MainActivity.this), R.mipmap.ic_launcher))
						.build ());


				break;
			case R.id.app_share_img_bytes:
				BitmapDrawable drawable = (BitmapDrawable) getResources ().getDrawable (R.drawable.example);
				Bitmap bitmap = drawable.getBitmap ();
				byte[] bytes = BitmapUtils.bitmap2Bytes (bitmap);

				shareDiffImage (new ShareAction.Builder ()
						.setTitle ("[bytes]分享 title 来自 bookbuf；")
						.setText ("[url] 分享 text 来自 bookbuf；")
						.setMedia (new ImageMedia (new WeakReference<Context> (MainActivity.this), bytes))
						.build ());


				break;
			default:
				break;
		}

	}

	public void shareDiffImage (ShareContent shareContent) {
		IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
			}
		};

		ShareAction action = new ShareAction (this)
				.setShareContent (shareContent)
				.setShareForm ("[B]www.healthbok.com")
				.setSharePlatform (platform)
				.setShareListener (shareListener);

		service.runShare (this, action, action.getShareListener ());
	}

	@Override
	protected void onActivityResult (int requestCode, int resultCode, Intent data) {
		super.onActivityResult (requestCode, resultCode, data);
		service.onActivityResult (requestCode, resultCode, data);
	}
}
