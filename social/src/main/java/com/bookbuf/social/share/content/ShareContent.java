package com.bookbuf.social.share.content;


import com.bookbuf.social.share.content.location.Location;
import com.bookbuf.social.share.content.media.IMediaObject;

/**
 * Created by robert on 16/6/29.
 */
public class ShareContent {

	public String mTitle;
	public String mTargetUrl;
	public String mText;
	public Location mLocation;
	public IMediaObject mMedia;
	public IMediaObject mExtra;
	public String mFollow;

	public ShareContent () {
	}
}
