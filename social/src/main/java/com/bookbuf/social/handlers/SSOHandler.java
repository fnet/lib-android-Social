package com.bookbuf.social.handlers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.bookbuf.social.platforms.IPlatform;

/**
 * Created by robert on 16/6/29.
 */
public abstract class SSOHandler<Platform extends IPlatform> implements ISSOHandler, ILifeCycleHandler<Platform>, IShareHandler {

	private Context context;
	private Platform platform;
	// 调起方
	private String callFrom;

	public String getCallFrom () {
		return callFrom;
	}

	public void setCallFrom (String callFrom) {
		this.callFrom = callFrom;
	}

	public Platform getPlatform () {
		return platform;
	}

	public void setPlatform (Platform platform) {
		this.platform = platform;
	}

	public Context getContext () {
		return context;
	}

	public void setContext (Context context) {
		this.context = context;
	}

	/**
	 * @param context
	 * @return 默认返回 true(该平台不支持查询);
	 */
	@Override
	public boolean isInstalled (Context context) {
		return true;
	}

	@Override
	public boolean isSupportAuthorize () {
		return false;
	}

	@Override
	public boolean isAuthorized (Context context) {
		return false;
	}

	@Override
	public void applyAuthorize (Activity activity, AuthListener listener) {

	}

	@Override
	public void deleteAuthorize (Context context, AuthListener listener) {

	}

	@Override
	public void onCreate (Context context, Platform platform) {
		this.context = context.getApplicationContext ();
		this.platform = platform;
	}

	@Override
	public void onActivityResult (int requestCode, int resultCode, Intent intent) {

	}

	@Override
	public void onNewIntent (Intent intent) {

	}

	public abstract int getRequestCode ();
}
