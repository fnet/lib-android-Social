package com.bookbuf.social.secure;

import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by robert on 16/7/4.
 */
public class AesHelper {

	private static byte[] pwd = null;
	private static byte[] iv = "nmeug.f9/Om+L823".getBytes ();
	private static final String UTF_8 = "UTF-8";
	private static boolean isEncryption = true;

	public AesHelper () {
	}

	public static String encryptNoPadding (String value, String charsetName) throws Exception {
		Cipher cipher = Cipher.getInstance ("AES/CBC/NoPadding");
		int blockSize = cipher.getBlockSize ();
		byte[] bytes = value.getBytes (charsetName);
		int length = bytes.length;
		if (length % blockSize != 0) {
			length += blockSize - length % blockSize;
		}

		byte[] buffer = new byte[length];
		System.arraycopy (bytes, 0, buffer, 0, bytes.length);
		SecretKeySpec aes = new SecretKeySpec (pwd, "AES");
		IvParameterSpec spec = new IvParameterSpec (iv);
		cipher.init (1, aes, spec);
		byte[] doFinal = cipher.doFinal (buffer);
		return new String (Base64.encode (doFinal, Base64.DEFAULT), UTF_8);
	}

	public static String decryptNoPadding (String value, String charsetName) throws Exception {
		Cipher cipher = Cipher.getInstance ("AES/CBC/NoPadding");
		SecretKeySpec aes = new SecretKeySpec (pwd, "AES");
		IvParameterSpec spec = new IvParameterSpec (iv);
		cipher.init (2, aes, spec);
		byte[] doFinal = cipher.doFinal (Base64.decode (value.getBytes (), Base64.DEFAULT));
		return new String (doFinal, charsetName);
	}

	public static void setPassword (String password) {
		if (!TextUtils.isEmpty (password)) {
			String md5 = md5 (password);
			if (md5.length () >= 16) {
				md5 = md5.substring (0, 16);
			}

			pwd = md5.getBytes ();
		}

	}

	public static byte[] getBytesUtf8 (String value) {
		return getBytesUnchecked (value, "UTF-8");
	}

	public static byte[] getBytesUnchecked (String value, String charsetName) {
		if (value == null) {
			return null;
		} else {
			try {
				return value.getBytes (charsetName);
			} catch (UnsupportedEncodingException e) {
				throw newIllegalStateException (charsetName, e);
			}
		}
	}

	private static IllegalStateException newIllegalStateException (String value, UnsupportedEncodingException e) {
		return new IllegalStateException (value + ": " + e);
	}

	public static String newString (byte[] values, String charsetName) {
		if (values == null) {
			return null;
		} else {
			try {
				return new String (values, charsetName);
			} catch (UnsupportedEncodingException e) {
				throw newIllegalStateException (charsetName, e);
			}
		}
	}

	public static String newStringUtf8 (byte[] values) {
		return newString (values, "UTF-8");
	}

	public static String md5 (String value) {
		if (value == null) {
			return null;
		} else {
			try {
				byte[] var1 = value.getBytes ();
				MessageDigest digest = MessageDigest.getInstance ("MD5");
				digest.reset ();
				digest.update (var1);
				byte[] bytes = digest.digest ();
				StringBuffer stringBuffer = new StringBuffer ();

				for (int i = 0; i < bytes.length; ++i) {
					stringBuffer.append (String.format ("%02X", new Object[]{Byte.valueOf (bytes[i])}));
				}

				return stringBuffer.toString ();
			} catch (Exception e) {
				return value.replaceAll ("[^[a-z][A-Z][0-9][.][_]]", "");
			}
		}
	}
}
